<?php
namespace App\Http\Services;
use App\Models\CarPhoto;
use Illuminate\Support\Facades\Storage;

class CarphotoDelete
{
    public function delete(CarPhoto $carphoto)
    {
        Storage::delete('public/images/'.$carphoto->image);       
        $carphoto->delete();
    }    
}