<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'brand' => 'required',
            'year' => 'required',
            'price' =>'required|integer|gt:0',
            'gearbox' => 'required',
            'emissions'=> 'required',
            //'images' => 'array:images',
            'images.*' => 'image|distinct|mimes:jpeg,jpg,bmp,png'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'brand.required' => 'Car brand is required!',
            'year.required'  => 'Year is required!',
            'price.required' => 'Car price is required!',
            'price.integer' => 'Price must be an integer value!',
            'year.required' => 'Car year is required!',
            'gearbox.required' => 'Please select gearbox!',
            'emissions.required'=> 'Please select emissions type!',
            'images.image' => 'Files uploaded must be jpeg,jpg,bmp,png type!'
        ];
    }
}
