<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\User;
use App\Models\CarPhoto;
use Inertia\Inertia;
use Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\UploadRequest;
use App\Http\Requests\UpdateRequest;
use Illuminate\Support\Facades\URL;
use App\Http\Services\CarPhotoDelete;

use Illuminate\Http\Request;

class CarController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cars = Car::with(['carphotos','user'])->when($request->brand, function($query, $brand){                
            return $query->where('brand','LIKE','%'.$brand.'%');
        })
        ->when($request->fromyear, function($query, $fromyear){
            return $query->where('year', '>=' ,$fromyear);
        })  
        ->when($request->toyear, function($query, $toyear){
            return $query->where('year', '<=' ,$toyear);
        })  
        ->when($request->pricefrom, function($query, $pricefrom){
            return $query->where('price', '>=' ,$pricefrom);
        })  
        ->when($request->priceto, function($query, $priceto){
            return $query->where('price', '<=' ,$priceto);
        }) 
        ->when($request->gearbox, function($query, $gearbox){                
            return $query->where('gearbox','LIKE','%'.$gearbox.'%');
        })
        ->when($request->emissions, function($query, $emissions){                
            return $query->where('emissions','LIKE','%'.$emissions.'%');
        })    
        ->when($request->service, function($query, $service){                
            return $query->where('service','LIKE','%'.$service.'%');
        })  
        ->when($request->filter=='desc', function($query, $filter){              
            return $query->orderBy('price', 'DESC');
        }) 
        ->when($request->filter=='asc', function($query, $filter){              
            return $query->orderBy('price', 'ASC');
        })
        ->when($request->filter=='newest', function($query, $filter){              
            return $query->orderBy('created_at', 'DESC');
        })
        ->when($request->filter=='oldest', function($query, $filter){              
            return $query->orderBy('created_at', 'ASC');
        })
        
        ->paginate(2)->withQueryString()->through(function ($car) { 

            return [                
                'id' => $car->id,
                'brand' => $car->brand,
                'price' => $car->price,
                'email' => $car->user->email,
                'photos' => $car->carphotos->map(function($carphoto) {                                
                                $carphoto->image = asset('/storage/images/'.$carphoto->image);
                                return $carphoto;
                            }),        
                'showurl' => URL::route('cars.show', $car->id)           
            ];
        });
        //dd($cars);
        return Inertia::render('Welcome', [
            'cars' =>  $cars,
            'filters' =>  $request->only(['brand','fromyear','toyear','pricefrom', 'priceto', 'gearbox','emissions','service','filter'])                       
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Cars/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UploadRequest $request)
    {
        $car = auth()->user()->cars()->create(request(['brand','year','price','gearbox','emissions','service','information']));

        if($request->hasFile('images')){
            $this->storeimages($car, $request->images);
        }
        
        return Redirect::route('mycars');
    }


    protected function storeimages($car, $images)
    {       
        foreach($images as $image){
            $ext = $image->getClientOriginalExtension();
            $filename = date('YmdHis').rand(1,99999).'.'.$ext;            
            $filepath = $image->storeAs('public/images',$filename);

            $carphoto = $car->carphotos()->create([
                'image'=>$filename,
                'car_id'=>$car->id
            ]);                    
        }
        return $carphoto;        
    }

    /**    
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        $car->load(['carphotos','user']);
        foreach($car->carphotos as $key =>$carphoto){
            $carphoto->image = asset('/storage/images/'.$carphoto->image);            
        }

        return Inertia::render('Cars/Show', ['car' => $car]);
    }

    public function mycars()
    {        
        $cars = Car::with(['carphotos','user'])->where('user_id','=', Auth::id())->paginate(2);
        
        foreach($cars as $key => $car){
            $cars[$key]->carphotos = $car->carphotos->map(function($carphoto)
                {
                    $carphoto->image = asset('/storage/images/'.$carphoto->image);
                    return $carphoto;
                });
        }
        //dd($cars);
        return Inertia::render('Cars/Mycars', ['cars' => $cars]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        $car->load('carphotos');
        foreach($car->carphotos as $key =>$carphoto){
            $carphoto->image = asset('/storage/images/'.$carphoto->image);            
        }

        return Inertia::render('Cars/Edit', ['car' => $car]);
    }

    public function destroyPicture(Carphoto $carphoto)    
    {       
                
       (new CarPhotoDelete())->delete($carphoto); 
        
        return Redirect::route('mycars');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //public function update(Request $request, $id)
    //{
        //
    //}
     
    public function updateCar(UpdateRequest $request, Car $car)
    {
        $car->update(request(['brand','year','price','gearbox','emissions','service','information']));

        if($request->hasFile('images')){
            $this->storeimages($car, $request->images);
        }

        return Redirect::route('mycars');
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        foreach( $car->carphotos as $carphoto ){           
            (new CarPhotoDelete())->delete($carphoto);
        }  
        $car->delete();
        return Redirect::route('mycars');
    }
}
