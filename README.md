# LaravelAutoMagJetInertia3


# How to use

- Clone the repository with git clone
- cp .env.example .env
- Edit .env file to find the database name
- Create the database. Use the database name from  .env  file
- Run composer install
- Run npm install
- Run npm run dev
- Run php artisan migrate --seed (it has some seeded data)
- Run php artisan key:generate
- Run php artisan serve
- That's it: launch the main URL(http://localhost:8000)
- If it is a problem with the pictures, delete storage folder from public and recreate it with: php artisan storage:link

# License
Basically, feel free to use and re-use any way you want.
