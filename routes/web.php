<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Car;
use App\Models\CarPhoto;
use App\Models\User;
use App\Http\Controllers\CarController;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
    $cars = Car::with(['carphotos','user'])->when($request->brand, function($query, $brand){                
        return $query->where('brand','LIKE','%'.$brand.'%');
    })
    ->when($request->pricefrom, function($query, $pricefrom){
        return $query->where('price', '>=' ,$pricefrom);
    })  
    ->when($request->priceto, function($query, $priceto){
        return $query->where('price', '<=' ,$priceto);
    }) 
    ->when($request->filter=='desc', function($query, $filter){              
        return $query->orderBy('price', 'DESC');
    }) 
    ->when($request->filter=='asc', function($query, $filter){              
        return $query->orderBy('price', 'ASC');
    })
    ->when($request->filter=='newest', function($query, $filter){              
        return $query->orderBy('created_at', 'DESC');
    })
    ->when($request->filter=='oldest', function($query, $filter){              
        return $query->orderBy('created_at', 'ASC');
    })

    ->paginate(2)->withQueryString()->through(function ($car) { 

        return [                
            'id' => $car->id,
            'brand' => $car->brand,
            'price' => $car->price,
            'email' => $car->user->email,
            'photos' => $car->carphotos->map(function($carphoto) {                                
                            $carphoto->image = asset('/storage/images/'.$carphoto->image);
                            return $carphoto;
                        }),        
            'showurl' => URL::route('cars.show', $car->id)           
        ];
    });
    //dd($cars);
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'cars' =>  $cars,
        'filters' =>  $request->only(['brand','pricefrom','priceto','filter'])             
    ]);
    
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
});

//Route::get('/users', [AdminController::class, 'index'])->middleware('admin')->name('users');

//Route::delete('/deleteuser/{user}',[AdminController::class, 'deleteUser'])->middleware('admin')->name('deleteuser');

//Route::get('/mycars', [CarController::class, 'mycars'])->middleware('user')->name('mycars');

//Route::delete('/destroypicture/{carphoto}',[CarController::class, 'destroyPicture'])->middleware('user')->name('destroypicture');

//Route::post('/updatecar/{car}', [CarController::class, 'updateCar'])->middleware('user')->name('updatecar');

//Route::resource('cars', CarController::class);

 
Route::middleware('admin')->group(function() {

    Route::get('/users', [AdminController::class, 'index'])->name('users');

    Route::delete('/deleteuser/{user}',[AdminController::class, 'deleteUser'])->name('deleteuser');
});

Route::middleware('user')->group(function() {

    Route::get('/mycars', [CarController::class, 'mycars'])->name('mycars');

    Route::delete('/destroypicture/{carphoto}',[CarController::class, 'destroyPicture'])->name('destroypicture');

    Route::post('/updatecar/{car}', [CarController::class, 'updateCar'])->name('updatecar');
    
});

Route::resource('cars', CarController::class);